const { override, fixBabelImports, addLessLoader } = require('customize-cra');

module.exports = override(
  fixBabelImports('import', {
      libraryName: 'antd',
      libraryDirectory: 'es',
      style: true,
    }),
    addLessLoader({
        javascriptEnabled: true,
        modifyVars: { 
          '@body-background': '#2D2D2D',
          '@font-family': 'Helvetica, Arial,  sans-serif',
          '@primary-color': '#b7b7b7',
          '@link-color': '#1890ff',
          '@btn-group-border': '#6f6f6f',
          '@font-size-base': '15px',
       },
    }),
);