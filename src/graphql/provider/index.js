import React from 'react'

//AppSync and Apollo libraries
import AWSAppSyncClient from "aws-appsync";
import { Rehydrated } from 'aws-appsync-react';
import { ApolloProvider } from 'react-apollo';

//Amplify
import { Auth } from 'aws-amplify';

import awsconfig from '../../aws-exports';

const GRAPHQL_API_REGION = awsconfig.aws_appsync_region
const GRAPHQL_API_ENDPOINT_URL = awsconfig.aws_appsync_graphqlEndpoint
const AUTH_TYPE = awsconfig.aws_appsync_authenticationType

// AppSync client instantiation
const client = new AWSAppSyncClient({
    url: GRAPHQL_API_ENDPOINT_URL,
    region: GRAPHQL_API_REGION,
    auth: {
        type: AUTH_TYPE,
        // Get the currently logged in users credential.
        jwtToken: async () => (await Auth.currentSession()).getAccessToken().getJwtToken(),
    },
    // Amplify uses Amazon IAM to authorize calls to Amazon S3. This provides the relevant IAM credentials.
    complexObjectsCredentials: () => Auth.currentCredentials()
})

export function withApolloProvider(WrappedComponent) {
    return class extends React.Component {
        render() {
            return  <ApolloProvider client={client}>
                        <Rehydrated>
                            <WrappedComponent {...this.props} />
                        </Rehydrated>
                    </ApolloProvider>;
        }
    };
}
