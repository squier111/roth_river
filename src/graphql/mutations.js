// eslint-disable
// this is an auto generated file. This will be overwritten

export const createCompany = `mutation CreateCompany($input: CreateCompanyInput!) {
  createCompany(input: $input) {
    id
    LegalID
    LegalName
    DBA
    StreetAddress1
    StreetAddress2
    City
    State
    Zipcode
    Country
    Phone
    URL
    Users {
      items {
        id
        Name
        Email
        Role
      }
      nextToken
    }
    Warehouses {
      items {
        id
        Name
        Nickname
        TotalCapacity
        Latitude
        Longitude
        NumberOfFlows
        NumberOfRowsPerFlow
        NumberOfBarrelsPerRow
      }
      nextToken
    }
  }
}
`;
export const updateCompany = `mutation UpdateCompany($input: UpdateCompanyInput!) {
  updateCompany(input: $input) {
    id
    LegalID
    LegalName
    DBA
    StreetAddress1
    StreetAddress2
    City
    State
    Zipcode
    Country
    Phone
    URL
    Users {
      items {
        id
        Name
        Email
        Role
      }
      nextToken
    }
    Warehouses {
      items {
        id
        Name
        Nickname
        TotalCapacity
        Latitude
        Longitude
        NumberOfFlows
        NumberOfRowsPerFlow
        NumberOfBarrelsPerRow
      }
      nextToken
    }
  }
}
`;
export const deleteCompany = `mutation DeleteCompany($input: DeleteCompanyInput!) {
  deleteCompany(input: $input) {
    id
    LegalID
    LegalName
    DBA
    StreetAddress1
    StreetAddress2
    City
    State
    Zipcode
    Country
    Phone
    URL
    Users {
      items {
        id
        Name
        Email
        Role
      }
      nextToken
    }
    Warehouses {
      items {
        id
        Name
        Nickname
        TotalCapacity
        Latitude
        Longitude
        NumberOfFlows
        NumberOfRowsPerFlow
        NumberOfBarrelsPerRow
      }
      nextToken
    }
  }
}
`;
export const createUser = `mutation CreateUser($input: CreateUserInput!) {
  createUser(input: $input) {
    id
    Name
    Email
    Role
    Company {
      id
      LegalID
      LegalName
      DBA
      StreetAddress1
      StreetAddress2
      City
      State
      Zipcode
      Country
      Phone
      URL
      Users {
        nextToken
      }
      Warehouses {
        nextToken
      }
    }
  }
}
`;
export const updateUser = `mutation UpdateUser($input: UpdateUserInput!) {
  updateUser(input: $input) {
    id
    Name
    Email
    Role
    Company {
      id
      LegalID
      LegalName
      DBA
      StreetAddress1
      StreetAddress2
      City
      State
      Zipcode
      Country
      Phone
      URL
      Users {
        nextToken
      }
      Warehouses {
        nextToken
      }
    }
  }
}
`;
export const deleteUser = `mutation DeleteUser($input: DeleteUserInput!) {
  deleteUser(input: $input) {
    id
    Name
    Email
    Role
    Company {
      id
      LegalID
      LegalName
      DBA
      StreetAddress1
      StreetAddress2
      City
      State
      Zipcode
      Country
      Phone
      URL
      Users {
        nextToken
      }
      Warehouses {
        nextToken
      }
    }
  }
}
`;
export const createWarehouse = `mutation CreateWarehouse($input: CreateWarehouseInput!) {
  createWarehouse(input: $input) {
    id
    Name
    Nickname
    TotalCapacity
    Latitude
    Longitude
    NumberOfFlows
    NumberOfRowsPerFlow
    NumberOfBarrelsPerRow
    Company {
      id
      LegalID
      LegalName
      DBA
      StreetAddress1
      StreetAddress2
      City
      State
      Zipcode
      Country
      Phone
      URL
      Users {
        nextToken
      }
      Warehouses {
        nextToken
      }
    }
  }
}
`;
export const updateWarehouse = `mutation UpdateWarehouse($input: UpdateWarehouseInput!) {
  updateWarehouse(input: $input) {
    id
    Name
    Nickname
    TotalCapacity
    Latitude
    Longitude
    NumberOfFlows
    NumberOfRowsPerFlow
    NumberOfBarrelsPerRow
    Company {
      id
      LegalID
      LegalName
      DBA
      StreetAddress1
      StreetAddress2
      City
      State
      Zipcode
      Country
      Phone
      URL
      Users {
        nextToken
      }
      Warehouses {
        nextToken
      }
    }
  }
}
`;
export const deleteWarehouse = `mutation DeleteWarehouse($input: DeleteWarehouseInput!) {
  deleteWarehouse(input: $input) {
    id
    Name
    Nickname
    TotalCapacity
    Latitude
    Longitude
    NumberOfFlows
    NumberOfRowsPerFlow
    NumberOfBarrelsPerRow
    Company {
      id
      LegalID
      LegalName
      DBA
      StreetAddress1
      StreetAddress2
      City
      State
      Zipcode
      Country
      Phone
      URL
      Users {
        nextToken
      }
      Warehouses {
        nextToken
      }
    }
  }
}
`;
