// eslint-disable
// this is an auto generated file. This will be overwritten

export const onCreateCompany = `subscription OnCreateCompany {
  onCreateCompany {
    id
    LegalID
    LegalName
    DBA
    StreetAddress1
    StreetAddress2
    City
    State
    Zipcode
    Country
    Phone
    URL
    Users {
      items {
        id
        Name
        Email
        Role
      }
      nextToken
    }
    Warehouses {
      items {
        id
        Name
        Nickname
        TotalCapacity
        Latitude
        Longitude
        NumberOfFlows
        NumberOfRowsPerFlow
        NumberOfBarrelsPerRow
      }
      nextToken
    }
  }
}
`;
export const onUpdateCompany = `subscription OnUpdateCompany {
  onUpdateCompany {
    id
    LegalID
    LegalName
    DBA
    StreetAddress1
    StreetAddress2
    City
    State
    Zipcode
    Country
    Phone
    URL
    Users {
      items {
        id
        Name
        Email
        Role
      }
      nextToken
    }
    Warehouses {
      items {
        id
        Name
        Nickname
        TotalCapacity
        Latitude
        Longitude
        NumberOfFlows
        NumberOfRowsPerFlow
        NumberOfBarrelsPerRow
      }
      nextToken
    }
  }
}
`;
export const onDeleteCompany = `subscription OnDeleteCompany {
  onDeleteCompany {
    id
    LegalID
    LegalName
    DBA
    StreetAddress1
    StreetAddress2
    City
    State
    Zipcode
    Country
    Phone
    URL
    Users {
      items {
        id
        Name
        Email
        Role
      }
      nextToken
    }
    Warehouses {
      items {
        id
        Name
        Nickname
        TotalCapacity
        Latitude
        Longitude
        NumberOfFlows
        NumberOfRowsPerFlow
        NumberOfBarrelsPerRow
      }
      nextToken
    }
  }
}
`;
export const onCreateUser = `subscription OnCreateUser {
  onCreateUser {
    id
    Name
    Email
    Role
    Company {
      id
      LegalID
      LegalName
      DBA
      StreetAddress1
      StreetAddress2
      City
      State
      Zipcode
      Country
      Phone
      URL
      Users {
        nextToken
      }
      Warehouses {
        nextToken
      }
    }
  }
}
`;
export const onUpdateUser = `subscription OnUpdateUser {
  onUpdateUser {
    id
    Name
    Email
    Role
    Company {
      id
      LegalID
      LegalName
      DBA
      StreetAddress1
      StreetAddress2
      City
      State
      Zipcode
      Country
      Phone
      URL
      Users {
        nextToken
      }
      Warehouses {
        nextToken
      }
    }
  }
}
`;
export const onDeleteUser = `subscription OnDeleteUser {
  onDeleteUser {
    id
    Name
    Email
    Role
    Company {
      id
      LegalID
      LegalName
      DBA
      StreetAddress1
      StreetAddress2
      City
      State
      Zipcode
      Country
      Phone
      URL
      Users {
        nextToken
      }
      Warehouses {
        nextToken
      }
    }
  }
}
`;
export const onCreateWarehouse = `subscription OnCreateWarehouse {
  onCreateWarehouse {
    id
    Name
    Nickname
    TotalCapacity
    Latitude
    Longitude
    NumberOfFlows
    NumberOfRowsPerFlow
    NumberOfBarrelsPerRow
    Company {
      id
      LegalID
      LegalName
      DBA
      StreetAddress1
      StreetAddress2
      City
      State
      Zipcode
      Country
      Phone
      URL
      Users {
        nextToken
      }
      Warehouses {
        nextToken
      }
    }
  }
}
`;
export const onUpdateWarehouse = `subscription OnUpdateWarehouse {
  onUpdateWarehouse {
    id
    Name
    Nickname
    TotalCapacity
    Latitude
    Longitude
    NumberOfFlows
    NumberOfRowsPerFlow
    NumberOfBarrelsPerRow
    Company {
      id
      LegalID
      LegalName
      DBA
      StreetAddress1
      StreetAddress2
      City
      State
      Zipcode
      Country
      Phone
      URL
      Users {
        nextToken
      }
      Warehouses {
        nextToken
      }
    }
  }
}
`;
export const onDeleteWarehouse = `subscription OnDeleteWarehouse {
  onDeleteWarehouse {
    id
    Name
    Nickname
    TotalCapacity
    Latitude
    Longitude
    NumberOfFlows
    NumberOfRowsPerFlow
    NumberOfBarrelsPerRow
    Company {
      id
      LegalID
      LegalName
      DBA
      StreetAddress1
      StreetAddress2
      City
      State
      Zipcode
      Country
      Phone
      URL
      Users {
        nextToken
      }
      Warehouses {
        nextToken
      }
    }
  }
}
`;
