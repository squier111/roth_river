import gql from 'graphql-tag';

export default gql`
    query {
        listWarehouses(limit: 100) {
            items {
                id
                Name
            }
        }
    }`;