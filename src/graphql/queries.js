// eslint-disable
// this is an auto generated file. This will be overwritten

export const getCompany = `query GetCompany($id: ID!) {
  getCompany(id: $id) {
    id
    LegalID
    LegalName
    DBA
    StreetAddress1
    StreetAddress2
    City
    State
    Zipcode
    Country
    Phone
    URL
    Users {
      items {
        id
        Name
        Email
        Role
      }
      nextToken
    }
    Warehouses {
      items {
        id
        Name
        Nickname
        TotalCapacity
        Latitude
        Longitude
        NumberOfFlows
        NumberOfRowsPerFlow
        NumberOfBarrelsPerRow
      }
      nextToken
    }
  }
}
`;
export const listCompanys = `query ListCompanys(
  $filter: ModelCompanyFilterInput
  $limit: Int
  $nextToken: String
) {
  listCompanys(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      LegalID
      LegalName
      DBA
      StreetAddress1
      StreetAddress2
      City
      State
      Zipcode
      Country
      Phone
      URL
      Users {
        nextToken
      }
      Warehouses {
        nextToken
      }
    }
    nextToken
  }
}
`;
export const getUser = `query GetUser($id: ID!) {
  getUser(id: $id) {
    id
    Name
    Email
    Role
    Company {
      id
      LegalID
      LegalName
      DBA
      StreetAddress1
      StreetAddress2
      City
      State
      Zipcode
      Country
      Phone
      URL
      Users {
        nextToken
      }
      Warehouses {
        nextToken
      }
    }
  }
}
`;
export const listUsers = `query ListUsers(
  $filter: ModelUserFilterInput
  $limit: Int
  $nextToken: String
) {
  listUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      Name
      Email
      Role
      Company {
        id
        LegalID
        LegalName
        DBA
        StreetAddress1
        StreetAddress2
        City
        State
        Zipcode
        Country
        Phone
        URL
      }
    }
    nextToken
  }
}
`;
export const getWarehouse = `query GetWarehouse($id: ID!) {
  getWarehouse(id: $id) {
    id
    Name
    Nickname
    TotalCapacity
    Latitude
    Longitude
    NumberOfFlows
    NumberOfRowsPerFlow
    NumberOfBarrelsPerRow
    Company {
      id
      LegalID
      LegalName
      DBA
      StreetAddress1
      StreetAddress2
      City
      State
      Zipcode
      Country
      Phone
      URL
      Users {
        nextToken
      }
      Warehouses {
        nextToken
      }
    }
  }
}
`;
export const listWarehouses = `query ListWarehouses(
  $filter: ModelWarehouseFilterInput
  $limit: Int
  $nextToken: String
) {
  listWarehouses(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      Name
      Nickname
      TotalCapacity
      Latitude
      Longitude
      NumberOfFlows
      NumberOfRowsPerFlow
      NumberOfBarrelsPerRow
      Company {
        id
        LegalID
        LegalName
        DBA
        StreetAddress1
        StreetAddress2
        City
        State
        Zipcode
        Country
        Phone
        URL
      }
    }
    nextToken
  }
}
`;
