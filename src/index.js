import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app';
import 'antd/dist/antd.css'; 
import {BrowserRouter as Router} from 'react-router-dom'
import ErrorBoundry from './components/error-boundry';
import Amplify from 'aws-amplify';
import awsconfig from './aws-exports';

Amplify.configure(awsconfig);

ReactDOM.render(
    <ErrorBoundry>
        <Router>
            <App/>
        </Router>
    </ErrorBoundry>,
  document.getElementById('root')
);
