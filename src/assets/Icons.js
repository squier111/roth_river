export { default as LOGO } from './img/SVG/logo.svg'
export { default as TTB } from './img/PNG/ttb.png'
export { default as INVENTORY } from './img/SVG/inventory.svg'
export { default as CALENDAR } from './img/SVG/calendar.svg'