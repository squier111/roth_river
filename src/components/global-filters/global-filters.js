import React, {Component} from 'react';
import warehouses from '../../graphql/quesries/warehouses';
import { graphql } from 'react-apollo';
import './global-filters.scss';
import { Menu, Dropdown } from 'antd';

console.log('warehouses', warehouses)

class GlobalFilter  extends Component {

  render() {

      let warehouses = []
      if (this.props.warehouses) {
          warehouses = this.props.warehouses.items
      }

      let customers = [
          {'id':4, 'Name':'Castle & Key'},
          {'id':5, 'Name':'Rabbit Hole'},
          {'id':6, 'Name':'Willett'},
      ]

      let types = [
          {'id':8, 'Name':'Bourbon'},
          {'id':9, 'Name':'Rye'},
          {'id':10, 'Name':'Others'},
      ]

      const locations = (
          <Menu>
              <Menu.Item key="0">
                  <a href="#">All Locations</a>
              </Menu.Item>
              {warehouses.map(warehouse => (
                  <Menu.Item key={ warehouse.id }>
                      <a href="#" key={ warehouse.id }>{ warehouse.Name }</a>
                  </Menu.Item>
              ))}
          </Menu>
      );

    const customer = (
      <Menu>
        <Menu.Item key="3">
          <a href="#">All Customers</a>
        </Menu.Item>
          {customers.map(customer => (
              <Menu.Item key={ customer.id }>
                  <a href="#" key={ customer.id }>{ customer.Name }</a>
              </Menu.Item>
          ))}
      </Menu>
    );
    const type = (
      <Menu>
        <Menu.Item key="7">
          <a href="#">All Types</a>
        </Menu.Item>
          {types.map(type => (
              <Menu.Item key={ type.id }>
                  <a href="#" key={ type.id }>{ type.Name }</a>
              </Menu.Item>
          ))}
      </Menu>
    );
    return (
      <div className="dropdown-holder">
        <Dropdown overlay={locations} trigger={['click']}>
          <a className="ant-dropdown-link" href="#">
            <span className="title">Locations</span>
            All Locations
          </a>
        </Dropdown>
        <Dropdown overlay={customer} trigger={['click']}>
          <a className="ant-dropdown-link" href="#">
            <span className="title">Customer</span>
            All Customers
          </a>
        </Dropdown>
        <Dropdown overlay={type} trigger={['click']}>
          <a className="ant-dropdown-link" href="#">
            <span className="title">type</span>
            All Types
          </a>
        </Dropdown>
      </div>
    );
  }
}

export default graphql(
    warehouses,
    {
        options: {
            fetchPolicy: 'cache-and-network',
        },
        props: ({ data: { listWarehouses: warehouses } }) => ({
            warehouses,
        })
    }
)(GlobalFilter);