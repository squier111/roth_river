import React, {Component} from 'react';
import { Row, Col, Select , Input } from 'antd';
import './filters.scss'
const Option = Select.Option;


class RangeFilter extends Component {
  handleChange(value) {
    console.log(`selected ${value}`);
  }
  render() {
    return (
     <div className="rangeFilter">
        <Row type="flex">
          <Col sm={24} md={24} lg={8} xl={8}>
            <label htmlFor="selectId">Select</label>
            <Select id="selectId" defaultValue="lucy" onChange={this.handleChange} size="large">
              <Option value="Lot#">Lot#</Option>
              <Option value="Serial #">Serial #</Option>
              <Option value="Batch #">Batch #</Option>
              <Option value="Fill Date">Fill Date</Option>
            </Select>
          </Col>
          <Col sm={24} md={24} lg={9} xl={9}>
            <label htmlFor="from">from range</label>
            <Input id="from" placeholder="Lot# Start" size="large" />
          </Col>
          <Col sm={24} md={24} lg={7} xl={7}>
            <label htmlFor="to">to</label>
            <Input id="to"  placeholder="Lot# End" size="large" />
          </Col>
        </Row>
     </div>
   );
  }
}

export default  RangeFilter;