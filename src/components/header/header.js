import React from 'react';
import './header.scss'
import { LOGO, TTB, INVENTORY, CALENDAR } from '../../assets/Icons'
import GlobalFilter from '../global-filters';
import Search from '../search';


const Header = () => {
  return (
    <header className="header">
        <div className="row-top">
          <a href="#" className="logo-holder">
            <img src={LOGO} alt='logo' className='logo' />
          </a>
          <ul className="header-list">
            <li>
                <a href="#">
                  <i className="icon">
                    <img src={INVENTORY} alt='inventory'/>
                  </i>
                  <span>Inventory</span> 
                </a>
            </li>
            <li>
                <a href="#">
                  <i className="icon">
                    <img src={TTB} alt='ttb'  />
                  </i>
                  <span>TTB</span>
                </a>
            </li>
            <li>
                <a href="#">
                  <i className="icon">
                    <img src={CALENDAR} alt='calendar'/>
                  </i>
                  <span>Pull Requests</span>
                </a>
            </li>
          </ul>
        </div>
        <div className="row-bottom">
          <div className="row-bottom-holder">
            <GlobalFilter/>
            <Search/>
          </div>
        </div>
    </header>
  )
}
export default  Header;