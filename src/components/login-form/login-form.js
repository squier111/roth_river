import React , {Component} from 'react';
import './login-form.scss';
import { LOGO } from '../../assets/Icons'
import {Form, Input, Button, Checkbox } from 'antd';

class LoginFormComponent extends Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="form-wrapper">
        <Form onSubmit={this.handleSubmit} className="login-form">
          <div className="logo-holder">
            <img src={LOGO} alt='logo' className='logo' />
          </div>
          <Form.Item
            label="Username"
            >
            {getFieldDecorator('userName', {
              rules: [{ required: true, message: 'Please input your username!' }],
            })(

              <Input placeholder="Username" />
            )}
          </Form.Item>
          <Form.Item
            label="Password"
            >
            {getFieldDecorator('password', {
              rules: [{ required: true, message: 'Please input your Password!' }],
            })(
              <Input type="password" placeholder="Password" />
            )}
          </Form.Item>
          <Form.Item className="row-checkbox">
            {getFieldDecorator('remember', {
              valuePropName: 'checked',
              initialValue: true,
            })(
              <Checkbox>Remember me</Checkbox>
            )}
            <a href="#" className="login-form-forgot" >Forgot password</a>
            <div className="submit-holder">
              <Button type="primary" htmlType="submit" className="login-form-button" onClick={this.props.Loginned}>
                Login
              </Button>
            </div>
            <div className ="description">
              <span>Trouble logging into your account? <a href="#">Get Help</a></span>
              <span className="copy">Copyright 2019 | <a href="#">Roth River Inc</a>  | <a href="#">Privacy</a> </span>
            </div>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

const LoginForm = Form.create({ name: 'normal_login' })(LoginFormComponent);

export default  LoginForm;