import React from 'react';
import './header-table.scss';
import { Row, Col, Button } from 'antd';
import UploadWarehouseCsv from '../upload-warehouse-csv';

const HeaderTable = () => {
   return (
    <header className="header-table">
        <Row>
          <Col span={24}>
            <span className="date">Last updated Mar 30, 2019</span>
          </Col>
        </Row>
        <Row>
          <Col sm={24} md={24} lg={18}>
            <h2>Castle & Key currently has <span>83,120 </span> Barrels in 2 Warehouses</h2>
          </Col>
          <Col sm={24} md={24} lg={6}>
              <UploadWarehouseCsv />
              {/* <Button icon="plus" className="btn-add">Add / Update Inventory</Button> */}
          </Col>
        </Row>
    </header>
  );
}

export default HeaderTable;