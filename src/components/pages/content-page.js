import React from 'react';
import ItemsTable from '../items-table';
import DataFilterTable from '../data-filter-table';
import HeaderTable from '../header-table';
import Mocks from '../../services/mocks';


const ContentPage =() => {
   const mocks = new Mocks();
   return (
      <div>
         <HeaderTable/>
         <DataFilterTable/>
         <ItemsTable mocks = {mocks} />
      </div>
  );
}

export default  ContentPage;