import LoginPage from './login-page';
import ContentPage from './content-page';

export {
  LoginPage,
  ContentPage
};