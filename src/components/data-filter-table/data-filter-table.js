import React , {Component} from 'react';
import './data-filter-table.scss';
import {RangeFilter , QuantityFilter} from '../filters';
import { Row, Col, Checkbox , Button , Radio  } from 'antd';
const RadioGroup = Radio.Group;


class DataFilterTable extends Component {

  constructor() {
    super();
    this.state = {
      toggleShowFilters : 1,
      value: 1,
    }
  }

  onChange = ({target}) => {
    const { toggleShowFilters } = this.state;
    if( toggleShowFilters === target.value) {
      return;
    }
    this.setState({
      toggleShowFilters: target.value,
      value: target.value,
    });
  }

  render() {
    const Filters = this.state.toggleShowFilters ? <RangeFilter/> :  <QuantityFilter/>;
    return (
       <div className="data-filter-table">
           <Row className="row-top" type="flex">
             <Col sm={24} md={24} lg={4} xl={4}>
               <ul className="result-list">
                 <li>
                     <span className="num">0</span>
                     <span className="name">Selected</span>
                 </li>
                 <li>
                     <span className="num">83,120</span>
                     <span className="name">Results</span>
                 </li>
               </ul>
               <Checkbox>Select all</Checkbox>
             </Col>
             <Col sm={12} md={12} lg={3} xl={3}>
               <span className="title">filter Type</span>
                <RadioGroup onChange={this.onChange} value={this.state.value}>
                  <Radio value={1} >Range</Radio>
                  <Radio value={0}>Quantity</Radio>
                </RadioGroup>
             </Col>
             <Col sm={12} md={12} lg={13} xl={13}>
               <span className="title">smart filter</span>
               {Filters}
             </Col>
             <Col sm={24} md={24} lg={4} xl={4}>
               <div className="btn-holder">
                 <Button className="btn-apply">Apply</Button>
               </div>
             </Col>
           </Row>
           <Row className="row-bottom" type="flex" justify="end">
               <Col span={8}>
                <Button type="primary">Add selected pull request</Button>
               </Col>
           </Row>
       </div>
   );
  }
}

export default  DataFilterTable;