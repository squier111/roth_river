import React from 'react';
import './search.scss';
import { Input , Icon  } from 'antd';


const Search = () => {
    return (
      <div className="search-holder">
       <Input
          prefix={<Icon type="search" style={{ color: '#7f7b77' }} />}
          placeholder="Search Anything"
        />
      </div>
    );
}

export  default Search;