import React, {Component} from 'react';
import 'antd/dist/antd.css'; 
import './app.scss';
import Header from '../header';
import {LoginPage, ContentPage} from '../pages';
import { withAuthenticator } from 'aws-amplify-react';
import { withApolloProvider } from '../../graphql/provider';

const Content = () => {
  return (
    <>
      <div className="header-center">
        <Header/>
      </div>
      <div className="center">
        <main role="main" className="main">
            <ContentPage/>
        </main>
      </div>
    </>
  )
}


class App extends Component {
  constructor() {
    super();
    this.state = {
      showLogin : false,
    }
  }

  Loginned = () => {
    this.setState({showLogin : false})
  }

  render () {
    const {showLogin} = this.state;

    const showPages = showLogin ? <LoginPage Loginned= {this.Loginned} /> : <Content/>;
    return (
      <div className="wrapper">
            {showPages}
      </div>
      )
    }
}

export default withAuthenticator(withApolloProvider(App, false));