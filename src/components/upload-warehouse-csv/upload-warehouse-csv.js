import React from 'react';
import { Icon } from 'antd';
import { parse } from 'papaparse';
import './upload-warehouse-csv.scss'

class UploadWarehouseCsv extends React.Component {
    constructor() {
        super();
        this.state = {
            csvfile: undefined
        };
        this.updateData = this.updateData.bind(this);
    }

    handleChange = event => {
        this.setState({
            csvfile: event.target.files[0]
        });
    };

    importCSV = () => {
        const { csvfile } = this.state;
        console.log('csvfile', csvfile)
        parse(csvfile, {
            complete: (result) => console.dir(result.data)
        });
    };

    updateData(result) {
        var data = result.data;
        console.log('updateData', data);
    }

    render() {
        console.log(this.state.csvfile);
        return (
            <div className="App">
                <label htmlFor="file" className="csv-input-holder">
                    <input
                        id="file"
                        className="csv-input"
                        type="file"
                        accept=".csv"
                        ref={input => {
                            this.filesInput = input;
                        }}
                        name="file"
                        placeholder={null}
                        onChange={this.handleChange}
                    />
                    <div className="btn-add"><Icon type="plus"/> Add / Update Inventory</div>
                    {/* <button onClick={this.importCSV}> Upload now!</button> */}
                </label>
            </div>
        );
    }
}

export default UploadWarehouseCsv;