import React ,{Component} from 'react';
import { Checkbox } from 'antd';
import './items-table.scss';



class ItemsTable extends Component {
  onChange(e) {
    console.log(`checked = ${e.target.checked}`);
  }
  RenderRow = (item) => {
    const {id, lot, idname , customer, serial, date , warehouse, location , lastMoved , action } = item;
    return(
      <tr key = {id}>
          <td><Checkbox onChange={this.onChange}></Checkbox></td>
          <td>{lot}</td>
          <td>{idname}</td>
          <td>{customer}</td>
          <td>{serial}</td>
          <td>{date}</td>
          <td>{warehouse}</td>
          <td>{location}</td>
          <td>{lastMoved}</td>
          <td>{action}</td>
      </tr>  
    )
  }
  render() {
    const {mocks} = this.props
    const items = mocks.items;
    return (
      <div className="items-table-holder">
        <table className="items-table">
          <thead>
            <tr>
              <th>Select</th>
              <th>Lot #</th>
              <th>Id</th>
              <th>Customer</th>
              <th>Serial #</th>
              <th>Fill Date</th>
              <th>Warehouse</th>
              <th>Location</th>
              <th>Last Moved</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {items.map(this.RenderRow)} 
          </tbody>
        </table>
      </div>
    )
  }
}



export default  ItemsTable;